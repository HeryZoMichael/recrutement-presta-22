# Test prestashop 1.7

Ce test nous permettra de comprendre **la méthodologie, réflexion et la capacité de programmation** du candidat.

*Le candidat doit traiter les deux parties du test.*

## Partie 1 : Mini projet sur prestashop 1.7

Nous créons une nouvelle base de données dans Prestashop, déjà paramétré dans le module

Nous souhaitons intégrer les webservices de Prestashop avec une version 1.7 et qu'ils puissent apparaître dans permission

Accessible pour faire un Create Read Update Delete

Réaliser un Create Read Update Delete sur les deux tables avec un Postman

### Attendu :

- Envoie du module modifié avec la nouvelle API visible dans les permissions
- Tous les droits activés automatiquement après installation
- Accessible par API
- Envoi d'un fichier Postman avec le CRUD

### Fichiers :
- HhApiSample.zip


## Partie 2 : Base de donnée et API

Considérer les deux requêtes sql suivantes, permettant de créer les deux tables ***'ps_quote'*** et ***'ps_quote_lang'***.

> CREATE TABLE IF NOT EXISTS `ps_quote` (id_quote INT AUTO_INCREMENT NOT NULL, author VARCHAR(255) NOT NULL, date_add DATETIME NOT NULL, date_upd DATETIME NOT NULL, isAttending int(11) DEFAULT NULL,isAttending1 int(11) DEFAULT NULL, id_product int(11) DEFAULT NULL,id_product1 int(11) DEFAULT NULL, PRIMARY KEY(id_quote)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

> CREATE TABLE IF NOT EXISTS `ps_quote_lang` (id_quote INT NOT NULL, id_lang INT NOT NULL, content LONGTEXT NOT NULL, link_read varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,description longtext COLLATE utf8_unicode_ci DEFAULT NULL,INDEX IDX_4D76F37E85F48AC4 (id_quote), INDEX IDX_4D76F37EBA299860 (id_lang), PRIMARY KEY(id_quote, id_lang)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB

### Demande :

- Créer la base de données et son API dans le module prestashop.
- Créer et exporter la collection de test correspondant, réalisée avec POSTMAN.


## Partie 3 : Front

On fournit les deux modules ***'modal-15'*** et ***'modal-18'*** :

### Demande :

- Afficher un lien texte à l'emplacement violet
- Sur le texte ouverture d'une popup
- Réaliser la popup du ***modal-15*** en changeant le background_image par une image
- Changer le bouton <Signup> (en orange) par les deux boutons <No thanks> & <Get in app> dans le ***modal-18***
- Il faut que les popups utilisent ces propre CSS & JS

### Fichiers :
- hook.png
- modal-15.zip
- modal-18.zip
